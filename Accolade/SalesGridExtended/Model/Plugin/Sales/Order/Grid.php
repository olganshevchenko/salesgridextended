<?php

namespace Accolade\SalesGridExtended\Model\Plugin\Sales\Order;

class Grid
{
    public static $leftJoinTable = 'sales_order';

    /**
     * Filter added columns using JOIN between ‘sales_order_grid’ and 'sales_order’
     */
    public function afterSearch($intercepter, $collection)
    {
        $leftJoinTable = $collection->getConnection()->getTableName(self::$leftJoinTable);
        $collection->getSelect()
                   ->joinLeft(
                       ['so' => $leftJoinTable],
                       'so.entity_id = main_table.entity_id',
                       ['so.coupon_code', 'so.discount_amount']
                   );
        $where = $collection->getSelect()->getPart(\Magento\Framework\DB\Select::WHERE);
        $collection->getSelect()->setPart(\Magento\Framework\DB\Select::WHERE, $where);

        return $collection;
    }
}
